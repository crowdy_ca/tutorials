//
//  ViewController.swift
//  NoStoryboard
//
//  Created by Ioan Adrian Hancu on 2015-02-08.
//  Copyright (c) 2015 CrowdY. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "Hello World"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

