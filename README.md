# Tutorials #

Note that the main repository is located on bitbucket (https://bitbucket.org/crowdy_ca/tutorials)
The GitHub one will always be up to date (https://github.com/caCrowdY/Tutorials)

When we push our changes they get pushed to both services.
We use both services in different ways when it comes to how we handle issue tracking.

### What is this repository for? ###

The objective behind this repository is to hold a knowledge base of tutorials to help new and veteran developers with commonly encountered development issues.

### Contribution guidelines ###

NOTE: ALL ISSUES ARE TO BE REPORTED ON GITHUB!
Bitbucket issue tracking is to ONLY be used by the developers.
We have set the bitbucket issue tracker to public for you to be able to follow what is going on.

You can currently help contribute to this project by:
* Suggest tutorials you would like to see
* Report broken tutorials

### How do I follow the tutorials up? ###

Each folder represents a tutorial.
The source files within the folder are the completed project
There is also a PDF document with instructions to help you accomplish and understand the tutorial.

### Who do I talk to? ###

Best way of getting in touch with us is by submitting an issue on GitHub